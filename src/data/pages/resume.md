---
date: "2016-02-16T22:15:08-06:00"
draft: false
title: "Resume"
type: resume
---

This page documents:

- my education at the University of Minnesota
- my places of employment, and roles
- my notable side projects
- my skillset &mdash; a list of technologies I have been exposed to and used
  - a set of buzzwords without context, please inquire

*Protip:* if you are recruiter, you get bonus points if you follow me and ping me [@ryanmr](https://twitter.com/ryanmr?recruiter).

### Education

I graduated from the University of Minnesota in 2015 with a Bachelors of Computer Science. In the Computer Science program, I was on the *Software Engineering*, *Systems* and *Compilers* track.

My original intent was leaning towards more web developer inclined coursework, but I was lucky enough to expand my horizons and come to love *compilers* and *systems*.

### Work Experience

#### Daugherty

I am a Consultant and Software Engineer 2 at Daugherty. My primary role is software engineering with a fullstack approach, and also consulting internal and external clients.

A [historical record of my work](/history) at Daugherty is available.
#### Saint Paul Public Schools

I helped instructors teach various Saint Paul Public Schools Community Education classes, such as Computer Basics 101, 201, Excel, PowerPoint and more. I also taught iterations of my own Website Basics and Website Construction classes, which focused on writing HTML, CSS and basic JavaScript by hand.

##### The Nexus

In 2011, I founded my [own small podcast network](http://thenexus.tv) and built my own small in-house podcasting studio. As of late 2017, I have recorded just over 650 episodes across various series.

During that same time, I have iterated upon the customized WordPress installation that powers the network content management system. Since early 2015, I have built prototypes of a successor Content Management System replacement in PHP with Laravel and Java with Spring Boot.

---

### Breakdown

I have experience with the following to varying levels. I consider these buzzwords and basically meaningless without the context of the projects. Despite that, some organizations believe buzzwords buy sales &mdash; read the following buzzwords as an exercise in asking useful questions. I cannot share details publically about the projects that used these specific technologies, but in confidence I may speak more about them privately.

#### Languages

- Java 7+
- PHP
  - PHP 5.4+
  - PHP 7+
- JavaScript
  - JavaScript
  - ES6+
  - TypeScript
- Rust
- Go
- HTML5
- YAML
- XML
- CSS
- SASS

#### JavaScript

- Vue
  - Vue CLI
  - Vue Router
  - Vuex
- Node 8+
- React
  - React Native
  - Redux
- Webpack
- Axios
- Lodash
- MooTools
- jQuery 3
- Knockout
- Gulp
- Browserify
- Angular
  - AngularJS 1.6+
  - Angular 2+

#### Java

- Spring
  - Spring 4
  - Spring Boot 1.6+
  - Spring Security
  - Spring Data
  - Data Rest
  - Spring Data Elastic Search
- Jackson JSON
- Android API 21+
- Maven
- Gradle

#### PHP

- WordPress 4.5+
- Laravel 5.3+
- Slim 3
- LAMP Stack

#### CI/CD
- Jenkins
- Drone.io

#### Design

- Bootstrap 4
- Foundation 6
- Axure

#### Database

- MySQL
- PostgreSQL
- DB2
- MongoDB
- Elastic Search

#### Tools

- IntelliJ
- DataGrip
- Slack
- Terminal
- Command Prompt
- Postman
- Git
- Github
- BitBucket
- GitLab
- VS Code
- Atom
- Sublime Text

#### Organization & Planning

- Trello
- Kanban Flow
- Jira
- VSTS

#### Cloud
- AWS
  - EC2
  - Elastic Beanstalk
  - Route 53
  - Cloudfront
  - S3
  - SES
- Kubernetes
- VPS
  - Linode VPS
  - Digital Ocean VPS

#### Services
- Auth0

#### Other
- Elastic Search
- Logstash
- Filebeat
- Chain Blockchain

#### Impressive Buzzwords & Keyword Jargon

Here's a list of meaningless out of context keyword jargon.

- Strategic Planning
- Process Improvement
- Workflow Enhancements
- Interdisciplinary Utilization
- Project Implementation Analysis
- Designing Multi-Service Applications
- Mentorship
- Troubleshooting