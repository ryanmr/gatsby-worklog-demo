---
date: "2016-02-17T19:17:27-06:00"
draft: false
title: "Made by Ryan"
type: made-by-ryan
---

You can visit the [Gitlab repository](https://gitlab.com/ryanmr/ryanrampersad.com). Changes are pushed to the repository, built by Gitlab and finally, deployed here.

#### Gatsby

I made this site with Gatsby. Gatsby is a static site generator with a unique approach. It uses React and JavaScript has a the templating mechanism, GraphQL as a data retreival system, and Webpack to combine all of the assets at build time.

#### Fonts

[League Gothic](https://www.theleagueofmoveabletype.com/league-gothic). Uppercased, text looks handsome and proud, but lean and agile.

#### Scripts

I adopted [Evan You's](http://evanyou.me/) [canvas effect](https://gist.github.com/ryanmr/205ef4297e7821fad088). I made the script a bit more extensible with my own tweaks, and I added the _motion_. You can see this modified version somewhere in the source control repository linked above.

#### Styles

I use [Bulma](https://bulma.io/) as the foundation for this site. It is clean and gets out of the way with no additional JavaScript requirements like jQuery.